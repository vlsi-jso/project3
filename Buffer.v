module Buffer (
	input in,
	output out
);

	wire temp;
	not not1 	(temp , in);
	not not2 	(out , temp);

endmodule

module tb_Buffer ();

	reg in;
	wire out;

	Buffer inst_Buffer (.in(in), .out(out));

	initial begin
		in = 1'b0;
		#20;
		in = 1'b1;
		$stop;
	end

endmodule