module tb();
	reg [15:0] a;
	reg [15:0] b;
	reg cin;
	wire [15:0] sum_brent;
	wire [15:0] sum_kogge;
	wire co_brent;
	wire co_kogge;
	integer seed1 = 1;
	integer seed2 = 2;

	Brent_Kung_Adder inst_Brent_Kung_Adder (.a(a), .b(b), .cin(cin), .sum(sum_brent), .cout(co_brent));

	Kogge_Stone_Adder inst_Kogge_Stone_Adder (.a(a), .b(b), .cin(cin), .sum(sum_kogge), .cout(co_kogge));

	initial begin
		repeat (10000) begin // this generate 10000 exaples.
			a = {$random(seed1)} % 20000;
			b = {$random(seed2)} % 20000;
			cin =  $random % 2;
			#100;
		end
		$stop;
	end

endmodule
