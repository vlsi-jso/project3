module Black_Cell (
	input Gik,
	input Pik,
	input Gk1j,
	input Pk1j,
	output Gij,
	output Pij
);


	wire and_out;
	and and1	(and_out , Gk1j , Pik );
	or  or1		(Gij ,  and_out , Gik);
	and and2 	(Pij , Pk1j , Pik);


endmodule

module tb_Black_Cell ();

	reg Gik, Pik, Gk1j, Pk1j;
	wire Gij, Pij;

	Black_Cell inst_Black_Cell (.Gik(Gik), .Pik(Pik), .Gk1j(Gk1j), .Pk1j(Pk1j), .Gij(Gij), .Pij(Pij));

	initial begin
		Gik  = 1'b0;
		Pik  = 1'b0;
		Gk1j = 1'b0;
		Pk1j = 1'b0;
		#20;
		Gik  = 1'b1;
		Pik  = 1'b0;
		Gk1j = 1'b0;
		Pk1j = 1'b0;
		#20;
		Gik  = 1'b0;
		Pik  = 1'b1;
		Gk1j = 1'b0;
		Pk1j = 1'b0;
		#20;
		Gik  = 1'b0;
		Pik  = 1'b0;
		Gk1j = 1'b1;
		Pk1j = 1'b0;
		#20;
		Gik  = 1'b0;
		Pik  = 1'b0;
		Gk1j = 1'b0;
		Pk1j = 1'b1;
		#20;
		Gik  = 1'b1;
		Pik  = 1'b1;
		Gk1j = 1'b0;
		Pk1j = 1'b0;
		#20;
		Gik  = 1'b1;
		Pik  = 1'b0;
		Gk1j = 1'b1;
		Pk1j = 1'b0;
		#20;
		Gik  = 1'b1;
		Pik  = 1'b0;
		Gk1j = 1'b0;
		Pk1j = 1'b1;
		#20;
		Gik  = 1'b0;
		Pik  = 1'b1;
		Gk1j = 1'b1;
		Pk1j = 1'b0;
		#20;
		Gik  = 1'b0;
		Pik  = 1'b1;
		Gk1j = 1'b0;
		Pk1j = 1'b1;
		#20;
		Gik  = 1'b0;
		Pik  = 1'b0;
		Gk1j = 1'b1;
		Pk1j = 1'b1;
		#20;
		Gik  = 1'b1;
		Pik  = 1'b1;
		Gk1j = 1'b1;
		Pk1j = 1'b0;
		#20;
		Gik  = 1'b1;
		Pik  = 1'b1;
		Gk1j = 1'b0;
		Pk1j = 1'b1;
		#20;
		Gik  = 1'b1;
		Pik  = 1'b0;
		Gk1j = 1'b1;
		Pk1j = 1'b1;
		#20;
		Gik  = 1'b0;
		Pik  = 1'b1;
		Gk1j = 1'b1;
		Pk1j = 1'b1;
		#20;
		Gik  = 1'b1;
		Pik  = 1'b1;
		Gk1j = 1'b1;
		Pk1j = 1'b1;
		$stop;
	end

endmodule