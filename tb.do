add wave a
add wave b
add wave cin
add wave sum_brent
add wave sum_kogge
add wave co_brent
add wave co_kogge
force a 16'd125
force b 16'd145
force cin 1'b0
run 100
force a 16'd299
force b 16'd333
force cin 1'b1
run 100
force a 16'd333
force b 16'd544
run 100
force a 16'd233
force b 16'd1423
run 100
force a 16'd12435
force b 16'd14225
run 100
force a 16'd113
force b 16'd0
force cin 1'b0
run 100
force a 16'd0
force b 16'd0
run 100
force a 16'd12533
force b 16'd14533
run 100
force a 16'd1255
force b 16'd5676
force cin 1'b1
run 100
force a 16'd1285
force b 16'd8145
run 100
force a 16'd8125
force b 16'd55145
run 100
