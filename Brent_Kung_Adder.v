module Brent_Kung_Adder (
	input 	[15:0] 	a,
	input 	[15:0] 	b,
	input  			cin,
	output 	[15:0]	sum,
	output 			cout
);

wire [15:0] p, g;
PG_Generator inst_PG_Generator (.a(a), .b(b), .p(p), .g(g));

// bit pos 0
wire buff_pos0_0;
Buffer inst_Buffer0 (.in(cin), .out(buff_pos0_0));


// bit pos 1
wire g_pos1_0, buff_pos1_1;
Gray_Cell inst_Gray_Cell1 (.Gik(g[0]), .Pik(p[0]), .Gk1j(cin), .Gij(g_pos1_0));
Buffer inst_Buffer1 (.in(g_pos1_0), .out(buff_pos1_1));


// bit pos 2
wire buff_pos2_0, g_pos2_1;
Buffer inst_Buffer2 (.in(p[1]), .out(buff_pos2_0));
Gray_Cell inst_Gray_Cell2 (.Gik(g[1]), .Pik(p[1]), .Gk1j(buff_pos2_0), .Gij(g_pos2_1));


// bit pos 3
wire g_pos3_0, p_pos3_0, g_pos3_1 , buff_pos3_2;
Black_Cell inst_Black_Cell3 (.Gik(g[2]), .Pik(p[2]), .Gk1j(g[1]), .Pk1j(p[1]), .Gij(g_pos3_0), .Pij(p_pos3_0));
Gray_Cell inst_Gray_Cell3 (.Gik(g_pos3_0), .Pik(p_pos3_0), .Gk1j(g_pos1_0), .Gij(g_pos3_1));
Buffer inst_Buffer3 (.in(g_pos3_1), .out(buff_pos3_2));


// bit pos 4
wire buff_pos4_0, g_pos4_1;
Buffer inst_Buffer4 (.in(p[3]), .out(buff_pos4_0));
Gray_Cell inst_Gray_Cell4 (.Gik(g[3]), .Pik(buff_pos4_0), .Gk1j(buff_pos3_2), .Gij(g_pos4_1));


// bit pos 5
wire g_pos5_0, p_pos5_0, buff_pos5_1, g_pos5_2, buff_pos5_3;
Black_Cell inst_Black_Cell5 (.Gik(g[4]), .Pik(p[4]), .Gk1j(g[3]), .Pk1j(p[3]), .Gij(g_pos5_0), .Pij(p_pos5_0));
Buffer inst_Buffer51 (.in(g_pos5_0), .out(buff_pos5_1));
Gray_Cell inst_Gray_Cell5 (.Gik(buff_pos5_1), .Pik(p_pos5_0), .Gk1j(buff_pos3_2), .Gij(g_pos5_2));
Buffer inst_Buffer52 (.in(g_pos5_2), .out(buff_pos5_3));


// bit pos 6
wire buff_pos6_0, g_pos6_1;
Buffer inst_Buffer6 (.in(g[5]), .out(buff_pos6_0));
Gray_Cell inst_Gray_Cell6 (.Gik(buff_pos6_0), .Pik(p[5]), .Gk1j(g_pos5_2), .Gij(g_pos6_1));


// bit pos 7
wire g_pos7_0, p_pos_7_0, g_pos7_1, p_pos7_1, g_pos7_2, buff_pos7_3;
Black_Cell inst_Black_Cell71 (.Gik(g[6]), .Pik(p[6]), .Gk1j(g[5]), .Pk1j(p[5]), .Gij(g_pos7_0), .Pij(p_pos_7_0));
Black_Cell inst_Black_Cell72 (.Gik(g_pos7_0), .Pik(p_pos_7_0), .Gk1j(g_pos5_0), .Pk1j(p_pos5_0), .Gij(g_pos7_1), .Pij(p_pos7_1));
Gray_Cell inst_Gray_Cell7 (.Gik(g_pos7_1), .Pik(p_pos7_1), .Gk1j(g_pos3_1), .Gij(g_pos7_2));
Buffer inst_Buffer7 (.in(g_pos7_2), .out(buff_pos7_3));


// bit pos 8
wire buff_pos8_0, g_pos8_1;
Buffer inst_Buffer8 (.in(g[7]), .out(buff_pos8_0));
Gray_Cell inst_Gray_Cell8 (.Gik(buff_pos8_0), .Pik(p[7]), .Gk1j(buff_pos7_3), .Gij(g_pos8_1));


// bit pos 9
wire g_pos9_0, p_pos9_0, buff_pos9_1, g_pos9_2, buff_pos9_3;
Black_Cell inst_Black_Cell9 (.Gik(g[8]), .Pik(p[8]), .Gk1j(g[7]), .Pk1j(p[7]), .Gij(g_pos9_0), .Pij(p_pos9_0));
Buffer inst_Buffer91 (.in(g_pos9_0), .out(buff_pos9_1));
Gray_Cell inst_Gray_Cell9 (.Gik(buff_pos9_1), .Pik(p_pos9_0), .Gk1j(buff_pos7_3), .Gij(g_pos9_2));
Buffer inst_Buffer92 (.in(g_pos9_2), .out(buff_pos9_3));


// bit pos 10
wire buff_pos10_0, g_pos10_1;
Buffer inst_Buffer10 (.in(g[9]), .out(buff_pos10_0));
Gray_Cell inst_Gray_Cell10 (.Gik(buff_pos10_0), .Pik(p[9]), .Gk1j(g_pos9_2), .Gij(g_pos10_1));


// bit pos 11
wire g_pos11_0, p_pos11_0, g_pos11_1, p_pos11_1, buff_pos11_2, g_pos11_3, buff_pos11_4;
Black_Cell inst_Black_Cell111 (.Gik(g[10]), .Pik(p[10]), .Gk1j(g[9]), .Pk1j(p[9]), .Gij(g_pos11_0), .Pij(p_pos11_0));
Black_Cell inst_Black_Cell112 (.Gik(g_pos11_0), .Pik(p_pos11_0), .Gk1j(g_pos9_0), .Pk1j(p_pos9_0), .Gij(g_pos11_1), .Pij(p_pos11_1));
Buffer inst_Buffer111 (.in(g_pos11_1), .out(buff_pos11_2));
Gray_Cell inst_Gray_Cell11 (.Gik(buff_pos11_2), .Pik(p_pos11_1), .Gk1j(buff_pos7_3), .Gij(g_pos11_3));
Buffer inst_Buffer112 (.in(g_pos11_3), .out(buff_pos11_4));


// bit pos 12
wire buff_pos12_0, g_pos12_1;
Buffer inst_Buffer12 (.in(g[11]), .out(buff_pos12_0));
Gray_Cell inst_Gray_Cell12 (.Gik(buff_pos12_0), .Pik(p[11]), .Gk1j(buff_pos11_4), .Gij(g_pos12_1));


// bit pos 13
wire g_pos13_0, p_pos13_0, buff_pos13_1, g_pos13_2, buff_pos13_3;
Black_Cell inst_Black_Cell13 (.Gik(g[12]), .Pik(p[12]), .Gk1j(g[11]), .Pk1j(p[11]), .Gij(g_pos13_0), .Pij(p_pos13_0));
Buffer inst_Buffer131 (.in(g_pos13_0), .out(buff_pos13_1));
Gray_Cell inst_Gray_Cell13 (.Gik(buff_pos13_1), .Pik(p_pos13_0), .Gk1j(g_pos11_3), .Gij(g_pos13_2));
Buffer inst_Buffer132 (.in(g_pos13_2), .out(buff_pos13_3));


// bit pos 14
wire buff_pos14_0, g_pos14_1;
Buffer inst_Buffer14 (.in(g[13]), .out(buff_pos14_0));
Gray_Cell inst_Gray_Cell14 (.Gik(buff_pos14_0), .Pik(p[13]), .Gk1j(g_pos13_2), .Gij(g_pos14_1));


// bit pos 15
wire g_pos15_0, p_pos15_0, g_pos15_1, p_pos15_1, g_pos15_2, p_pos15_2, g_pos15_3, buff_pos15_4;
Black_Cell inst_Black_Cell151 (.Gik(g[14]), .Pik(p[14]), .Gk1j(g[13]), .Pk1j(p[13]), .Gij(g_pos15_0), .Pij(p_pos15_0));
Black_Cell inst_Black_Cell152 (.Gik(g_pos15_0), .Pik(p_pos15_0), .Gk1j(g_pos13_0), .Pk1j(p_pos13_0), .Gij(g_pos15_1), .Pij(p_pos15_1));
Black_Cell inst_Black_Cell153 (.Gik(g_pos15_1), .Pik(p_pos15_1), .Gk1j(g_pos11_1), .Pk1j(p_pos11_1), .Gij(g_pos15_2), .Pij(p_pos15_2));
Gray_Cell inst_Gray_Cell15 (.Gik(g_pos15_2), .Pik(p_pos15_2), .Gk1j(g_pos7_2), .Gij(g_pos15_3));
Buffer inst_Buffer15 (.in(g_pos15_3), .out(buff_pos15_4));


assign sum = {buff_pos15_4, g_pos14_1, buff_pos13_3, g_pos12_1, buff_pos11_4, g_pos10_1, buff_pos9_3, g_pos8_1, buff_pos7_3, g_pos6_1, buff_pos5_3, g_pos4_1, buff_pos3_2, g_pos2_1, buff_pos1_1, buff_pos0_0} ^ p;

assign cout = g[15] | (p[15] & g_pos15_3);

endmodule