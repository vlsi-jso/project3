module Gray_Cell (
	input Gik,
	input Pik,
	input Gk1j,
	output Gij
);

	wire and_out;
	and and1	(and_out , Gk1j , Pik );
	or  or1		(Gij ,  and_out , Gik);

endmodule

module tb_Gray_Cell();

	reg Gik, Pik, Gk1j;
	wire Gij;

	Gray_Cell inst_Gray_Cell (.Gik(Gik), .Pik(Pik), .Gk1j(Gk1j), .Gij(Gij));

	initial begin
		Gik  = 1'b0;
		Pik  = 1'b0;
		Gk1j = 1'b0;
		#20;
		Gik  = 1'b1;
		Pik  = 1'b0;
		Gk1j = 1'b0;
		#20;
		Gik  = 1'b0;
		Pik  = 1'b1;
		Gk1j = 1'b0;
		#20;
		Gik  = 1'b0;
		Pik  = 1'b0;
		Gk1j = 1'b1;
		#20;
		Gik  = 1'b1;
		Pik  = 1'b0;
		Gk1j = 1'b1;
		#20;
		Gik  = 1'b1;
		Pik  = 1'b1;
		Gk1j = 1'b0;
		#20;
		Gik  = 1'b0;
		Pik  = 1'b1;
		Gk1j = 1'b1;
		#20;
		Gik  = 1'b1;
		Pik  = 1'b1;
		Gk1j = 1'b1;
		$stop;
	end


endmodule