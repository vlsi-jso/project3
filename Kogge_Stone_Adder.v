module Kogge_Stone_Adder (
	input 	[15:0] 	a,
	input 	[15:0] 	b,
	input  			cin,
	output 	[15:0]	sum,
	output 			cout
);

wire [15:0] p, g;
PG_Generator inst_PG_Generator (.a(a), .b(b), .p(p), .g(g));

// bit pos 0
wire buff_pos0_0;
Buffer inst_Buffer0 (.in(cin), .out(buff_pos0_0));

// bit pos 1
wire g_pos1_0, buff_pos1_1;
Gray_Cell inst_Gray_Cell1 (.Gik(g[0]), .Pik(p[0]), .Gk1j(cin), .Gij(g_pos1_0));
Buffer inst_Buffer1 (.in(g_pos1_0), .out(buff_pos1_1));

// bit pos 2
wire g_pos2_0, p_pos2_0, g_pos2_1, buff_pos2_2;
Black_Cell inst_Black_Cell2 (.Gik(g[1]), .Pik(p[1]), .Gk1j(g[0]), .Pk1j(p[0]), .Gij(g_pos2_0), .Pij(p_pos2_0));
Gray_Cell inst_Gray_Cell2 (.Gik(g_pos2_0), .Pik(p_pos2_0), .Gk1j(buff_pos0_0), .Gij(g_pos2_1));
Buffer inst_Buffer2 (.in(g_pos2_1), .out(buff_pos2_2));

// bit pos 3
wire g_pos3_0, p_pos3_0, g_pos3_1, buff_pos3_2;
Black_Cell inst_Black_Cell3 (.Gik(g[2]), .Pik(p[2]), .Gk1j(g[1]), .Pk1j(p[1]), .Gij(g_pos3_0), .Pij(p_pos3_0));
Gray_Cell inst_Gray_Cell3 (.Gik(g_pos3_0), .Pik(p_pos3_0), .Gk1j(g_pos1_0), .Gij(g_pos3_1));
Buffer inst_Buffer3 (.in(g_pos3_1), .out(buff_pos3_2));

// bit pos 4
wire g_pos4_0, p_pos4_0, g_pos4_1, p_pos4_1, g_pos4_2, buff_pos4_3;
Black_Cell inst_Black_Cell41 (.Gik(g[3]), .Pik(p[3]), .Gk1j(g[2]), .Pk1j(p[2]), .Gij(g_pos4_0), .Pij(p_pos4_0));
Black_Cell inst_Black_Cell42 (.Gik(g_pos4_0), .Pik(p_pos4_0), .Gk1j(g_pos2_0), .Pk1j(p_pos2_0), .Gij(g_pos4_1), .Pij(p_pos4_1));
Gray_Cell inst_Gray_Cell4 (.Gik(g_pos4_1), .Pik(p_pos4_1), .Gk1j(buff_pos0_0), .Gij(g_pos4_2));
Buffer inst_Buffer4 (.in(g_pos4_2), .out(buff_pos4_3));

// bit pos 5
wire g_pos5_0, p_pos5_0, g_pos5_1, p_pos5_1, g_pos5_2, buff_pos5_3;
Black_Cell inst_Black_Cell51 (.Gik(g[4]), .Pik(p[4]), .Gk1j(g[3]), .Pk1j(p[3]), .Gij(g_pos5_0), .Pij(p_pos5_0));
Black_Cell inst_Black_Cell52 (.Gik(g_pos5_0), .Pik(p_pos5_0), .Gk1j(g_pos3_0), .Pk1j(p_pos3_0), .Gij(g_pos5_1), .Pij(p_pos5_1));
Gray_Cell inst_Gray_Cell5 (.Gik(g_pos5_1), .Pik(p_pos5_1), .Gk1j(buff_pos1_1), .Gij(g_pos5_2));
Buffer inst_Buffer5 (.in(g_pos5_2), .out(buff_pos5_3));

// bit pos 6
wire g_pos6_0, p_pos6_0, g_pos6_1, p_pos6_1, g_pos6_2, buff_pos6_3;
Black_Cell inst_Black_Cell61 (.Gik(g[5]), .Pik(p[5]), .Gk1j(g[4]), .Pk1j(p[4]), .Gij(g_pos6_0), .Pij(p_pos6_0));
Black_Cell inst_Black_Cell62 (.Gik(g_pos6_0), .Pik(p_pos6_0), .Gk1j(g_pos4_0), .Pk1j(p_pos4_0), .Gij(g_pos6_1), .Pij(p_pos6_1));
Gray_Cell inst_Gray_Cell6 (.Gik(g_pos6_1), .Pik(p_pos6_1), .Gk1j(g_pos2_1), .Gij(g_pos6_2));
Buffer inst_Buffer6 (.in(g_pos6_2), .out(buff_pos6_3));

// bit pos 7
wire g_pos7_0, p_pos7_0, g_pos7_1, p_pos7_1, g_pos7_2, buff_pos7_3;
Black_Cell inst_Black_Cell71 (.Gik(g[6]), .Pik(p[6]), .Gk1j(g[5]), .Pk1j(p[5]), .Gij(g_pos7_0), .Pij(p_pos7_0));
Black_Cell inst_Black_Cell72 (.Gik(g_pos7_0), .Pik(p_pos7_0), .Gk1j(g_pos5_0), .Pk1j(p_pos5_0), .Gij(g_pos7_1), .Pij(p_pos7_1));
Gray_Cell inst_Gray_Cell7 (.Gik(g_pos7_1), .Pik(p_pos7_1), .Gk1j(g_pos3_1), .Gij(g_pos7_2));
Buffer inst_Buffer7 (.in(g_pos7_2), .out(buff_pos7_3));

// bit pos 8
wire g_pos8_0, p_pos8_0, g_pos8_1, p_pos8_1, g_pos8_2, p_pos8_2, g_pos8_3;
Black_Cell inst_Black_Cell81 (.Gik(g[7]), .Pik(p[7]), .Gk1j(g[6]), .Pk1j(p[6]), .Gij(g_pos8_0), .Pij(p_pos8_0));
Black_Cell inst_Black_Cell82 (.Gik(g_pos8_0), .Pik(p_pos8_0), .Gk1j(g_pos6_0), .Pk1j(p_pos6_0), .Gij(g_pos8_1), .Pij(p_pos8_1));
Black_Cell inst_Black_Cell83 (.Gik(g_pos8_1), .Pik(p_pos8_1), .Gk1j(g_pos4_1), .Pk1j(p_pos4_1), .Gij(g_pos8_2), .Pij(p_pos8_2));
Gray_Cell inst_Gray_Cell8 (.Gik(g_pos8_2), .Pik(p_pos8_2), .Gk1j(buff_pos0_0), .Gij(g_pos8_3));

// bit pos 9
wire g_pos9_0, p_pos9_0, g_pos9_1, p_pos9_1, g_pos9_2, p_pos9_2, g_pos9_3;
Black_Cell inst_Black_Cell91 (.Gik(g[8]), .Pik(p[8]), .Gk1j(g[7]), .Pk1j(p[7]), .Gij(g_pos9_0), .Pij(p_pos9_0));
Black_Cell inst_Black_Cell92 (.Gik(g_pos9_0), .Pik(p_pos9_0), .Gk1j(g_pos7_0), .Pk1j(p_pos7_0), .Gij(g_pos9_1), .Pij(p_pos9_1));
Black_Cell inst_Black_Cell93 (.Gik(g_pos9_1), .Pik(p_pos9_1), .Gk1j(g_pos5_1), .Pk1j(p_pos5_1), .Gij(g_pos9_2), .Pij(p_pos9_2));
Gray_Cell inst_Gray_Cell9 (.Gik(g_pos9_2), .Pik(p_pos9_2), .Gk1j(buff_pos1_1), .Gij(g_pos9_3));

// bit pos 10
wire g_pos10_0, p_pos10_0, g_pos10_1, p_pos10_1, g_pos10_2, p_pos10_2, g_pos10_3;
Black_Cell inst_Black_Cell101 (.Gik(g[9]), .Pik(p[9]), .Gk1j(g[8]), .Pk1j(p[8]), .Gij(g_pos10_0), .Pij(p_pos10_0));
Black_Cell inst_Black_Cell102 (.Gik(g_pos10_0), .Pik(p_pos10_0), .Gk1j(g_pos8_0), .Pk1j(p_pos8_0), .Gij(g_pos10_1), .Pij(p_pos10_1));
Black_Cell inst_Black_Cell103 (.Gik(g_pos10_1), .Pik(p_pos10_1), .Gk1j(g_pos6_1), .Pk1j(p_pos6_1), .Gij(g_pos10_2), .Pij(p_pos10_2));
Gray_Cell inst_Gray_Cell10 (.Gik(g_pos10_2), .Pik(p_pos10_2), .Gk1j(buff_pos2_2), .Gij(g_pos10_3));

// bit pos 11
wire g_pos11_0, p_pos11_0, g_pos11_1, p_pos11_1, g_pos11_2, p_pos11_2, g_pos11_3;
Black_Cell inst_Black_Cell111 (.Gik(g[10]), .Pik(p[10]), .Gk1j(g[9]), .Pk1j(p[9]), .Gij(g_pos11_0), .Pij(p_pos11_0));
Black_Cell inst_Black_Cell112 (.Gik(g_pos11_0), .Pik(p_pos11_0), .Gk1j(g_pos9_0), .Pk1j(p_pos9_0), .Gij(g_pos11_1), .Pij(p_pos11_1));
Black_Cell inst_Black_Cell113 (.Gik(g_pos11_1), .Pik(p_pos11_1), .Gk1j(g_pos7_1), .Pk1j(p_pos7_1), .Gij(g_pos11_2), .Pij(p_pos11_2));
Gray_Cell inst_Gray_Cell11 (.Gik(g_pos11_2), .Pik(p_pos11_2), .Gk1j(buff_pos3_2), .Gij(g_pos11_3));

// bit pos 12
wire g_pos12_0, p_pos12_0, g_pos12_1, p_pos12_1, g_pos12_2, p_pos12_2, g_pos12_3;
Black_Cell inst_Black_Cell121 (.Gik(g[11]), .Pik(p[11]), .Gk1j(g[10]), .Pk1j(p[10]), .Gij(g_pos12_0), .Pij(p_pos12_0));
Black_Cell inst_Black_Cell122 (.Gik(g_pos12_0), .Pik(p_pos12_0), .Gk1j(g_pos10_0), .Pk1j(p_pos10_0), .Gij(g_pos12_1), .Pij(p_pos12_1));
Black_Cell inst_Black_Cell123 (.Gik(g_pos12_1), .Pik(p_pos12_1), .Gk1j(g_pos8_1), .Pk1j(p_pos8_1), .Gij(g_pos12_2), .Pij(p_pos12_2));
Gray_Cell inst_Gray_Cell12 (.Gik(g_pos12_2), .Pik(p_pos12_2), .Gk1j(g_pos4_2), .Gij(g_pos12_3));

// bit pos 13
wire g_pos13_0, p_pos13_0, g_pos13_1, p_pos13_1, g_pos13_2, p_pos13_2, g_pos13_3;
Black_Cell inst_Black_Cell131 (.Gik(g[12]), .Pik(p[12]), .Gk1j(g[11]), .Pk1j(p[11]), .Gij(g_pos13_0), .Pij(p_pos13_0));
Black_Cell inst_Black_Cell132 (.Gik(g_pos13_0), .Pik(p_pos13_0), .Gk1j(g_pos11_0), .Pk1j(p_pos11_0), .Gij(g_pos13_1), .Pij(p_pos13_1));
Black_Cell inst_Black_Cell133 (.Gik(g_pos13_1), .Pik(p_pos13_1), .Gk1j(g_pos9_1), .Pk1j(p_pos9_1), .Gij(g_pos13_2), .Pij(p_pos13_2));
Gray_Cell inst_Gray_Cell13 (.Gik(g_pos13_2), .Pik(p_pos13_2), .Gk1j(g_pos5_2), .Gij(g_pos13_3));

// bit pos 14
wire g_pos14_0, p_pos14_0, g_pos14_1, p_pos14_1, g_pos14_2, p_pos14_2, g_pos14_3;
Black_Cell inst_Black_Cell141 (.Gik(g[13]), .Pik(p[13]), .Gk1j(g[12]), .Pk1j(p[12]), .Gij(g_pos14_0), .Pij(p_pos14_0));
Black_Cell inst_Black_Cell142 (.Gik(g_pos14_0), .Pik(p_pos14_0), .Gk1j(g_pos12_0), .Pk1j(p_pos12_0), .Gij(g_pos14_1), .Pij(p_pos14_1));
Black_Cell inst_Black_Cell143 (.Gik(g_pos14_1), .Pik(p_pos14_1), .Gk1j(g_pos10_1), .Pk1j(p_pos10_1), .Gij(g_pos14_2), .Pij(p_pos14_2));
Gray_Cell inst_Gray_Cell14 (.Gik(g_pos14_2), .Pik(p_pos14_2), .Gk1j(g_pos6_2), .Gij(g_pos14_3));

// bit pos 15
wire g_pos15_0, p_pos15_0, g_pos15_1, p_pos15_1, g_pos15_2, p_pos15_2, g_pos15_3;
Black_Cell inst_Black_Cell151 (.Gik(g[14]), .Pik(p[14]), .Gk1j(g[13]), .Pk1j(p[13]), .Gij(g_pos15_0), .Pij(p_pos15_0));
Black_Cell inst_Black_Cell152 (.Gik(g_pos15_0), .Pik(p_pos15_0), .Gk1j(g_pos13_0), .Pk1j(p_pos13_0), .Gij(g_pos15_1), .Pij(p_pos15_1));
Black_Cell inst_Black_Cell153 (.Gik(g_pos15_1), .Pik(p_pos15_1), .Gk1j(g_pos11_1), .Pk1j(p_pos11_1), .Gij(g_pos15_2), .Pij(p_pos15_2));
Gray_Cell inst_Gray_Cell15 (.Gik(g_pos15_2), .Pik(p_pos15_2), .Gk1j(g_pos7_2), .Gij(g_pos15_3));

assign sum = {g_pos15_3, g_pos14_3, g_pos13_3, g_pos12_3, g_pos11_3, g_pos10_3, g_pos9_3, g_pos8_3, buff_pos7_3, buff_pos6_3, buff_pos5_3, buff_pos4_3, buff_pos3_2, buff_pos2_2, buff_pos1_1, buff_pos0_0} ^ p;

assign cout = g[15] | (p[15] & g_pos15_3);

endmodule