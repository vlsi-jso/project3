module PG_Generator (
	input 	[15:0]	a,
	input 	[15:0]	b,
	output 	[15:0] 	p,
	output 	[15:0] 	g
);

	wire [16:0] temp_g;
	wire [16:0] temp_p;

	genvar i;
    for (i=1 ;i<17 ; i=i+1) begin
      assign temp_g[i] = a[i-1] & b[i-1]; 
      assign temp_p[i] = a[i-1] ^ b[i-1];
    end

    assign p = temp_p[16:1];
    assign g = temp_g[16:1];

endmodule

module tb_PG_Generator ();

	reg [15:0] a, b;
	wire [15:0] p, g;

	PG_Generator inst_PG_Generator (.a(a), .b(b), .p(p), .g(g));

	initial begin
		a = 16'b1111111111111111;
		b = 16'b0000000000000001;
		#20;
		a = 16'b1111111111111100;
		b = 16'b0000000000000001;
		$stop;
	end

endmodule